#!/usr/bin/env python3

from PIL import Image
import os
import time
import warnings

def current_milli_time():
  return round(time.time() * 1000)

# benchmark_name = 'blur_downscaled_limited'
# benchmark_name = 'blur_batched_downscaled_pretrained'
# benchmark_name = 'blur_yolo_precise@blur_downscaled_limited'
benchmark_name = 'blur_yolo_precise@blur_batched_downscaled_pretrained'

### Not included
# benchmark_name = 'blur_original'
# benchmark_name = 'blur_downscaled'
# benchmark_name = 'blur_downscaled_pretrained'
# benchmark_name = 'blur_batched_pretrained'
# benchmark_name = 'blur_yolo'
# benchmark_name = 'blur_yolo_noparts@blur_batched_downscaled_pretrained'
# benchmark_name = 'blur_yolo_noparts@blur_downscaled_limited'
# benchmark_name = 'blur_yolo_inspect'

input_folder = 'dataset'
output_folder = 'output'
pictures_process_count = -1 # set to -1 to process all

benchmark_output_folder = os.path.join(output_folder, benchmark_name)
time_file = os.path.join(benchmark_output_folder, 'times.txt')

def blur_picture(picture, blur, model):
  if hasattr(blur, 'compute_blur_mask'):
    blur_mask = blur.compute_blur_mask(picture, model)
    blurred_image = Image.new("RGBA", picture.size, (255,0,0,255))
    # blurred_image = picture.filter(ImageFilter.GaussianBlur(radius=30))
    picture.paste(blurred_image, (0, 0), blur_mask)
    return picture
  else:
    return blur.blur_picture(picture, model)

def run():
  global pictures_process_count
  blur = __import__(benchmark_name.split('@')[0], globals(), locals())
  blur.benchmark_name = benchmark_name

  if not os.path.exists(input_folder):
    os.mkdir(input_folder)
  if not os.path.exists(output_folder):
    os.mkdir(output_folder)
  if not os.path.exists(benchmark_output_folder):
    os.mkdir(benchmark_output_folder)
  for filename in os.listdir(benchmark_output_folder):
    file_path = os.path.join(benchmark_output_folder, filename)
    os.unlink(file_path)

  print("---- Preparing model", benchmark_name, "----")
  model = blur.prepare_model()
  print("---- Running model", benchmark_name, "----")

  execution_timestamp = current_milli_time()

  for filename in os.listdir(input_folder):
    if '.jpg' not in filename:
      continue

    print("Running on", filename)
    timestamp = current_milli_time()
    picture = Image.open(os.path.join(input_folder, filename))
    blured_image = blur_picture(picture, blur, model)
    blured_image.save(os.path.join(benchmark_output_folder, filename), format="jpeg", quality=90, subsampling=0)
    deltatime = current_milli_time() - timestamp
    print("/Running on %s time: %ss" % (filename, deltatime/1000))

    with open(time_file, 'a') as f:
      f.write("%s %f\n" % (filename, deltatime/1000))

    pictures_process_count -= 1
    if pictures_process_count == 0:
      break

  execution_time = current_milli_time() - execution_timestamp
  print("Done with the benchmarks, total time: %ss" % (execution_time/1000))
  with open(time_file, 'a') as f:
    f.write("total %f\n" % (execution_time/1000))


if __name__ == '__main__':
  run()