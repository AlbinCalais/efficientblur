"""
This script uses a Tensorflow segmentation model,
the input image is scalled down to fit in a quad of 
513px in either width or height, it is divided into
513x513px quads that are fed to the model and the output
is combined to blur the original image.
Most of the time 2 quads are sufficient to blur a full
picture.

This script is somewhat fast (~20s per image) and
achieves good but not perfect accuracy.

If your computer supports it, you may remove the first
few lines of this file to allow the model to use all
available cpu resources. It will make the model faster
but may crash the program if it tries to take too much
ram.
"""

import os

# REMOVE TO UNLIMIT
limit = f"{2}" # limit maximum number of cpu threads active at the same time
os.environ["OMP_NUM_THREADS"] = limit
os.environ['TF_NUM_INTEROP_THREADS'] = limit
os.environ['TF_NUM_INTRAOP_THREADS'] = limit
#

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # disable non-error logs

import urllib.request
import tarfile
import os.path
import image_pre

import numpy as np

from PIL import Image, ImageDraw, ImageFilter

from packaging import version

import tensorflow.compat.v1 as tf

def getModelFile(modelName, downloadUrl=None):
    """Returns the model file associated with the model's name.

    If necessary, this method will download the model file.

    Parameters
    ----------
    modelName : str
        the model's name
    downloadUrl : str | None
        if the model file does not exist it will be downloaded.
        if downloadUrl is None an exception will be thrown 

    Returns
    -------
    str
        the model file path
    """
    modelsDir = os.path.join(os.path.dirname(__file__), 'models')
    downloadPath = os.path.join(modelsDir, modelName)
    if not os.path.exists(downloadPath):
        if downloadUrl is None:
            raise Exception("Model %s does not exist and cannot be downloaded" % modelName)
        print("Downloading model", modelName)
        tf.gfile.MakeDirs(modelsDir)
        urllib.request.urlretrieve(downloadUrl, downloadPath + ".tmp")
        os.rename(downloadPath + ".tmp", downloadPath)
    return downloadPath

class DeepLabModel(object):
    """Class to load deeplab model and run inference."""

    MODEL_NAME = 'deeplabv3_pascal_trainval_2018_01_04.tar.gz'
    DOWNLOAD_URL = 'http://download.tensorflow.org/models/deeplabv3_pascal_trainval_2018_01_04.tar.gz'

    LABEL_NAMES = [
        'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
        'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog',
        'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa',
        'train', 'tv'
    ]

    BLURRED_CLASSES = [
        'bus', 'car', 'motorbike', 'person'
    ]

    FROZEN_GRAPH_NAME = 'frozen_inference_graph'
    INPUT_TENSOR_NAME = 'ImageTensor:0'
    OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
    INPUT_SIZE = 513

    def __init__(self):
        """Creates and loads pretrained deeplab model."""
        self.graph = tf.Graph()
        graph_def = None
        tar_file = tarfile.open(getModelFile(self.MODEL_NAME, self.DOWNLOAD_URL))
        for tar_info in tar_file.getmembers():
            if self.FROZEN_GRAPH_NAME in os.path.basename(tar_info.name):
                file_handle = tar_file.extractfile(tar_info)
                graph_def = tf.GraphDef.FromString(file_handle.read())
                break

        tar_file.close()

        if graph_def is None:
            raise RuntimeError('Cannot find inference graph in tar archive.')

        with self.graph.as_default():
            tf.import_graph_def(graph_def, name='')

        self.sess = tf.Session(graph=self.graph)

    def run(self, resized_image):
        """Runs inference on a single image.

        Args:
            image: A PIL.Image object, raw input image.

        Returns:
            segmentation_map: Segmentation map of `resized_image`.
        """
        batch_segmentation_map = self.sess.run(
            self.OUTPUT_TENSOR_NAME,
            feed_dict={self.INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
        segmentation_map = batch_segmentation_map[0]
        return segmentation_map

def prepare_model():
    return DeepLabModel()

def compute_blur_mask(picture, model):
    blur_colormap = np.zeros((512,4), dtype=int)
    for clazz in DeepLabModel.BLURRED_CLASSES:
        index = DeepLabModel.LABEL_NAMES.index(clazz)
        blur_colormap[index] = (255,255,255,255)
    disable_downscale = False
    blur_mask = Image.new('RGBA', picture.size, (0,0,0,0))
    target_size = (model.INPUT_SIZE, model.INPUT_SIZE)
    for x,y,img in image_pre.iter_scaled_sections_of_image(picture, target_size, disable_downscale):
        segmentation_map = model.run(img)
        segmentation_mask_scaled = Image.fromarray(np.uint8(blur_colormap[segmentation_map]))
        segx, segy, segmentation_mask = image_pre.upscale_scaled_section(picture, target_size, x, y, segmentation_mask_scaled, disable_downscale)
        blur_mask.paste(segmentation_mask, (segx,segy), segmentation_mask)
    return blur_mask
