import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import os
import matplotlib.backends.backend_pdf
import math

###
### A scripts that takes the output images from the benchmarks
### and makes a pdf out of theim, for human quality comparison
### purpose
###

# Watch your RAM usage! images are read at full quality
# and kept in memory until a pdf is generated.

def open_small_image(image_path):
  image = Image.open(image_path)
  m = max(image.size[0]/800, image.size[1]/450)
  if m > 1:
    return image.resize((int(image.size[0]/m), int(image.size[1]/m)), Image.Resampling.LANCZOS)
  return image

def time_to_str(seconds_str):
  seconds = float(seconds_str)
  if seconds > 3600:
    return "%dh%dm%fs" % (seconds//3600, (seconds%3600)//60, seconds%1)
  elif seconds > 60:
    return "%dm%fs" % (seconds//60, seconds%1)
  else:
    return "%fs" % seconds


input_folder = 'dataset'
benchmarks_folder = 'output'
pdfdir = 'compare'
image_per = 10 # lower is ram usage is a problem

expectedPictureCount = len(os.listdir(input_folder))

if not os.path.exists(pdfdir):
  os.mkdir(pdfdir)

benchmark_names = []
benchmark_times = {}
for dirname in os.listdir(benchmarks_folder):
  timefile = os.path.join(benchmarks_folder, dirname, 'times.txt')
  if os.path.exists(timefile):
    benchmark_names.append(dirname)
    with open(timefile, 'r') as file:
      times = file.readlines()[:-1]
      registered_times = {}
      for l in times:
        [ imagename, time ] = l.split(' ')
        registered_times[imagename] = time
      benchmark_times[dirname] = registered_times
      if len(times) != expectedPictureCount:
        print("Expected", expectedPictureCount, "times, got", len(times), "for benchmark", dirname)
      
for filename in os.listdir(pdfdir):
  file_path = os.path.join(pdfdir, filename)
  if not os.path.isdir(file_path):
    os.unlink(file_path)


print("Displaying results of benchmarks\n ", '\n  '.join(benchmark_names))

def show_batch(imgname, imgindex):
  print("image", imgname)
  N = len(benchmark_names)+1
  R = math.floor(math.sqrt(N))
  C = math.ceil(N/R)
  fig, axes = plt.subplots(R, C, squeeze=False)
  for ax in axes.flatten():
    ax.axis('off')

  originalimage = open_small_image(os.path.join(input_folder, imgname))
  ax = axes[0,0]
  ax.imshow(originalimage)
  ax.set_title("original (%dx%d)" % originalimage.size, { 'fontsize': 5 })
  ax.axis('off')

  for i, benchmark in enumerate(benchmark_names):
    imagefilepath = os.path.join(benchmarks_folder, benchmark, imgname)
    if not os.path.exists(imagefilepath):
      print("Image does not exist: benchmark=", benchmark, "img=", imgname)
    else:
      image = open_small_image(imagefilepath)
      ax = axes[(i+1)//C, (i+1)%C]
      ax.imshow(image)
      ax.set_title("%s\n%s" % (benchmark.replace('@', '\n'), time_to_str(benchmark_times[benchmark][imgname].rstrip())), {'fontsize': 5 })
  # plt.show()
  pdf.savefig(fig, dpi=300)
  plt.close('all')

image_index = 0
pdf = matplotlib.backends.backend_pdf.PdfPages(os.path.join(pdfdir, "output_0.pdf"))
for imgname in os.listdir(input_folder):
  if '.jpg' in imgname:
    show_batch(imgname, image_index)
    image_index += 1
    if image_index % image_per == 0:
      pdf.close()
      print("Wrote pdf")
      pdf = matplotlib.backends.backend_pdf.PdfPages(os.path.join(pdfdir, "output_%d.pdf" % (image_index//image_per)))
pdf.close()
print("Wrote pdf")
  