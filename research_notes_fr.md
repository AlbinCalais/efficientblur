## Pour optimiser le floutage il y a plusieurs pistes :
- prétraiter l'image (pour donner moins au modèle)
- changer complètement de modèle
- optimiser les appels au modèle

### Comme pré-traitement on peut imaginer :
- réduire l'échelle de l'image (si le modèle prend des images de 500x500px en entrée, on réduira une image e 3000x2000px en deux blocks de 500x500px)
- utiliser une IA de détection d'objets (style YOLOv6) pour identifier les zones dans lesquelles il y a des personnes/véhicules et ne passer que celles-la au modèle

#### Réduction de l'échelle
Sur les tests que j'ai mené on pouvait sans problème passer d'une image en 3000x2000 à deux images de 500x500 et le modèle réagissait bien.
J'imagine que ca dépend surtout de la taille originale des images sur lesquelles le modèle s'est entrainé.

#### Détection d'objets
L'idée serait d'identifier les véhicules/personnes en amont, et d'extraire une ou plusieurs images de l'originale qui ne contiennent que ceux-ci.
On peut par exemple utiliser [YOLOv6](https://github.com/meituan/YOLOv6) qui est très performant.
Une fois qu'on a les zones à traiter on peut les regrouper sur une seule image, pour ca j'ai utilisé [rectangle packer](https://pypi.org/project/rectangle-packer/).
Ca n'a pas l'air de géner le modèle s'il y a de grosses discontinuités dans l'image.
Par contre dans mes tests ce n'était pas compatible avec la réduction d'échelle, les rendus sont trop mauvaix avec.

### Pour changer de modèle il y a plusieurs sources :
- les modèles Tensorflow pré-entrainés, on en trouve sur [TensorflowHub](https://tfhub.dev/s?module-type=image-segmentation&subtype=module,placeholder)
- les modèles Pytorch, il y en a moins pour la segmentation mais on en trouve quand même [ici](https://pytorch.org/vision/stable/models.html#table-of-all-available-semantic-segmentation-weights)
- on peut aussi en entrainer un. Dans ce cas l'idéal serait de l'entrainer sur un jeu de données qui différencie les personnes et les plaques d'immatriculation. On pourrait même l'entrainer directement sur des images 360 mais le jeu de données risque d'être compliqué à trouver. On peut aussi en sur-entrainer un qui existe déjà.

Le modèle original est [ici](https://averdones.github.io/real-time-semantic-image-segmentation-with-deeplab-in-tensorflow/) et celui que j'ai trouvé qui est beaucoup plus rapide mais bien moins précis est [ici](https://tfhub.dev/sayakpaul/lite-model/mobilenetv2-dm05-coco/dr/1)

### Pour optimiser les appels au modèle :
On a pas beaucoup d'options possible, avec Tensorflow on peut envoyer plusieurs images d'un coup au modèle, par batch.
Ca ne marche que sur certains modèles. Le batching permet d'éviter les allers-retours entre le code python et le driver du modèle, sur mes tests les performances étaient meilleures mais la différence était négligeable.
Il y en a aussi qui supportent le redimensionnement de leur entrées, si on peut passer des images plus grandes directement au modèle ca pourrait être plus rapide? je n'en ai pas trouvé avec lesquels c'était possible.
Avec Pytorch le batching est aussi une possibilité mais je n'ai pas vérifié le gain en performances.

Certains modèles peuvent aussi être utilisés par plusieurs threads. Si le modèle est léger mais lent ca vaut vraiment le coup d'ajouter du multithreading.
Le modèle original est beaucoup trop gourmant en cpu pour que ca vaille le coup mais le deuxième était suffisament léger pour qu'on puisse le faire tourner sur plusieurs threads.
Par contre ce modèle-ci ne peut pas être partagé, il faut donc en charger une instance n

## Images 360
Pour traiter les images 360 on peut soit les passer telles quelles au modèle soit d'abord en extraire des images à plat, les passer au modèle et les re-projeter sur l'originale.
Si le modèle supporte les déformations c'est mieux de ne pas toucher à l'image originale.
Pour la projection il y a [cet article](https://omdena.com/blog/3d-object-detection/), mais les resultats étaient assez mauvaix lorsque j'ai essayé : l'algo ne supporte pas les images très grandes et la projection n'est probablement pas la bonne.

## Ce qui a bien fonctionné

1. donner l'image au detecteur d'objets
1. en extraire les zones (leurs bounding boxes) d'interets pour le segmenter
1. grouper les bbox qui se chevauchent
1. placer ces zones sur une nouvelle image, de préférence carré
1. donner cette image à un segmenter pour calculer les masques de floutage
1. *cumuler* les masques de floutage
1. appliquer le floutage

**Ce qui reste à tenter**:

Remplacer l'implémentation Pytorch de YOLO avec une de Tensorflow pour économiser de la ram ; on change forcément de version, par exemple en utilisant la [v3](https://github.com/mystic123/tensorflow-yolo-v3) et il n'y a pas d'assurance que la qualité/les performances soient les mêmes.

Faire des batch plus importants : faire tourner le detecteur d'objets sur une série d'images puis traiter la série avec le segmenteur. Ca évite d'avoir les deux modèles en même temps en mémoire mais ca demande plus de travail pour retrouver quelle bbox correspond à quelle image et plus d'écritures/lectures sur disque.