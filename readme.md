## Efficient blur

This project is an attempt at making an efficient blurring algorithm, using sementic segmentation AI and image processing.

What is to blur can be changed but by default persons, cars, bus and motorcycles are blurred.

Each `blur_xx.py` file is a different algorithm for blurring a picture. 

## Installation

Note: The scripts were tested with Python 3.9, they may or may not work with other python versions.

First, clone the [YOLOv6](https://github.com/meituan/YOLOv6) repository somewhere on your system, and download the [yolov6s.pt](https://github.com/meituan/YOLOv6/releases/download/0.1.0/yolov6s.pt) weights file and place it in the `YOLOv6` directory.

If you are running on windows remove the last line of the `requirements.txt` file and install the latest Pytorch version from [here](https://pytorch.org/) (latest build - windows - pip - python - CPU).

Then run
```
pip install -r requirements.txt
```

**Running**:

Set your `YOLOV6_PATH` environment variable to point to the YOLOv6 folder and put your images in the `dataset/` folder (create it if it does not exist).

```bash
# to run benchmarks
python3 benchmark.py
# to compare benchmarks
python3 compare.py
```

You may choose the algorithm to run by changing `benchmark_name` in the `benchmark.py` script.

You will get tensorflow errors saying cuda is not installed and pytorch deprecation warnings, do not pay attention to those.

### Notes

If your computer supports it, you may remove the first few lines of `blur_downscaled_limited` to allow it to use every available cpu.

The 360 images produce longer processing time because of the sheer size of the file, but most scripts (at least the YOLO ones) handle theim well.

Currently the most accurate algorithm is `blur_downscaled_limited`, the YOLO version is almost as precise and is also faster on large images.

These are POCs, the code is not final.
