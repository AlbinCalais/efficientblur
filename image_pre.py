"""
Image preprocessing script, needs refactoring
"""

from PIL import Image

def iter_quad_sub_sections(quad_width, quad_height, block_width, block_height, overlap):
  x = 0
  while x+block_width <= quad_width:
    y = 0
    while y+block_height <= quad_height:
      yield (x, y, block_width, block_height)
      y += block_height-overlap
    x += block_width-overlap
  if quad_width%(block_width-overlap)-overlap != 0:
    y = 0
    while y+block_height <= quad_height:
      yield (quad_width-block_width, y, block_width, block_height)
      y += block_height-overlap
  if quad_height%(block_height-overlap)-overlap != 0:
    x = 0
    while x+block_width <= quad_width:
      yield (x, quad_height-block_height, block_width, block_height)
      x += block_width-overlap
  if quad_width%(block_width-overlap)-overlap != 0 and quad_height%(block_height-overlap)-overlap != 0:
    yield (quad_width-block_width, quad_height-block_height, block_width, block_height)

def scale_size(size, scale):
    if type(scale) is not tuple:
        scale = (scale, scale)
    return (int(size[0]*scale[0]), int(size[1]*scale[1]))

def iter_scaled_sections_of_image(original_image, target_size, disable_downscale=False):
  downscale = 1 if disable_downscale else max(target_size[0]/original_image.size[0], target_size[1]/original_image.size[1])
  downscaled_image = original_image.resize(scale_size(original_image.size, downscale), Image.Resampling.LANCZOS)
  overlap = int(min(target_size)*.1) # 10% overlap
  for (x,y,w,h) in iter_quad_sub_sections(downscaled_image.size[0], downscaled_image.size[1], target_size[0], target_size[1], overlap):
    yield (x,y,downscaled_image.crop((x,y,x+w,y+h)))
  
def upscale_scaled_section(original_image, target_size, x, y, section, disable_downscale=False):
  downscale = 1 if disable_downscale else max(target_size[0]/original_image.size[0], target_size[1]/original_image.size[1])
  downscaled_size = scale_size(original_image.size, downscale)
  upscale_factor = (original_image.size[0]/downscaled_size[0], original_image.size[1]/downscaled_size[1])
  new_size = (int(section.size[0]*upscale_factor[0]), int(section.size[1]*upscale_factor[1]))
  return (int(x*upscale_factor[0]), int(y*upscale_factor[1]), section.resize(new_size, Image.Resampling.LANCZOS))

def upscale_scaled_size(original_image, target_size, x, y, w, h, disable_downscale=False):
  downscale = 1 if disable_downscale else max(target_size[0]/original_image.size[0], target_size[1]/original_image.size[1])
  downscaled_size = scale_size(original_image.size, downscale)
  upscale_factor = (original_image.size[0]/downscaled_size[0], original_image.size[1]/downscaled_size[1])
  new_coord = scale_size((x, y), upscale_factor)
  new_size = scale_size((w, h), upscale_factor)
  return new_coord, new_size
  