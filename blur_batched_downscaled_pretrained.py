"""
This script uses a Tensorflow Lite segmentation model,
the input image is scalled down to fit in a quad of 
513px in either width or height, it is divided into
513x513px quads that are fed to the model and the output
is combined to blur the original image.
Most of the time 2 quads are sufficient to blur a full
picture.

This script is realy fast (3-10s per image, depending
on the aspect ratio) but cannot achieve great precision
on larger images.
"""

import os
import urllib.request
import tarfile
import os.path
import image_pre

import numpy as np
import copy

from PIL import Image, ImageDraw, ImageFilter

from packaging import version

import tensorflow.compat.v1 as tf

model_name, download_path, LABEL_NAMES, blurred_classes = (
    "lite-model_mobilenetv2-dm05-coco_dr_1.tflite",
    'https://tfhub.dev/sayakpaul/lite-model/mobilenetv2-dm05-coco/dr/1?lite-format=tflite',
    [ 'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tv' ],
    [ 'bus', 'car', 'person', 'motorbike' ]
)

def getModelFile(modelName, downloadUrl=None):
    """Returns the model file associated with the model's name.

    If necessary, this method will download the model file.

    Parameters
    ----------
    modelName : str
        the model's name
    downloadUrl : str | None
        if the model file does not exist it will be downloaded.
        if downloadUrl is None an exception will be thrown 

    Returns
    -------
    str
        the model file path
    """
    modelsDir = os.path.join(os.path.dirname(__file__), 'models')
    downloadPath = os.path.join(modelsDir, modelName)
    if not os.path.exists(downloadPath):
        if downloadUrl is None:
            raise Exception("Model %s does not exist and cannot be downloaded" % modelName)
        print("Downloading model", modelName)
        # tf.gfile.MakeDirs(modelsDir)
        if not os.path.exists(modelsDir):
            os.mkdir(modelsDir)
        urllib.request.urlretrieve(downloadUrl, downloadPath + ".tmp")
        os.rename(downloadPath + ".tmp", downloadPath)
    return downloadPath

class DeepLabModel(object):
    """Class to load deeplab model and run inference."""

    BATCH_SIZE = 2

    def __init__(self):
        """Creates and loads pretrained deeplab model."""
        interpreter = tf.lite.Interpreter(model_path=getModelFile(model_name, download_path))
        # Set model input.
        input_details = interpreter.get_input_details()
        tensor_index = input_details[0]['index']
        tensor_shape = input_details[0]['shape']
        tensor_shape[0] = DeepLabModel.BATCH_SIZE
        interpreter.resize_tensor_input(tensor_index, tensor_shape)
        interpreter.allocate_tensors()

        self.input_size = tensor_shape[2], tensor_shape[1]
        self.interpreter = interpreter
        self.null_input_image = np.zeros((tensor_shape[2], tensor_shape[1], tensor_shape[3]))
        # self.output_tensor_index = self.interpreter.get_output_details()[0]['index']

    def run(self, images):
        print("Trying to run on", len(images))
        # append null images to respect the batch size
        while len(images) % DeepLabModel.BATCH_SIZE != 0:
          images.append(self.null_input_image)

        input_images = []
        segmentation_maps = []
        for image in images:
            input_image = np.asarray(image).astype(np.float32)
            input_image = input_image / 127.5 - 1
            input_images.append(input_image)
        for batch_index in range(0, len(images), DeepLabModel.BATCH_SIZE):
            print("Running batch of", DeepLabModel.BATCH_SIZE)
            batch = input_images[batch_index:batch_index+DeepLabModel().BATCH_SIZE]
            self.interpreter.set_tensor(self.interpreter.get_input_details()[0]['index'], batch)
            self.interpreter.invoke()
            raw_prediction = self.interpreter.get_tensor(self.interpreter.get_output_details()[0]['index'])
            for part_prediction in raw_prediction:
                seg_map = tf.argmax(part_prediction, axis=2)
                seg_map = tf.squeeze(seg_map).numpy().astype(np.int8)
                segmentation_maps.append(seg_map)
        # splice to remove the null images results
        return segmentation_maps[0:len(images)]

def prepare_model():
    return DeepLabModel()

def compute_blur_mask(picture, model):
    blur_colormap = np.zeros((512,4), dtype=int)
    for clazz in blurred_classes:
        index = LABEL_NAMES.index(clazz)
        blur_colormap[index] = (255,255,255,255)
    disable_downscale = False
    blur_mask = Image.new('RGBA', picture.size, (0,0,0,0))
    target_size = model.input_size
    image_parts = list(image_pre.iter_scaled_sections_of_image(picture, target_size, disable_downscale)) # [(x,y,img)]
    segmentation_maps = model.run([part[2] for part in image_parts])
    for (x,y,img),segmentation_map in zip(image_parts, segmentation_maps):
        segmentation_mask_scaled = Image.fromarray(np.uint8(blur_colormap[segmentation_map]))
        segx, segy, segmentation_mask = image_pre.upscale_scaled_section(picture, target_size, x, y, segmentation_mask_scaled, disable_downscale)
        blur_mask.paste(segmentation_mask, (segx,segy), segmentation_mask)
    return blur_mask
