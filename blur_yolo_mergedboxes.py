"""
Same as blur_yolo_precise but will try to merge
detected bounding boxes to feed the least possible
amount of data to the segmenter. Also the object
detection model won't run if the image is too small,
see the NO_DETECTION_THRESHOLD constant.
"""

import sys
import os

YOLOV6_PATH = os.environ['YOLOV6_PATH']
sys.path.append(YOLOV6_PATH)

import numpy as np
import cv2
import torch
import math
import rpack
from PIL import Image

from yolov6.utils.events import load_yaml
from yolov6.layers.common import DetectBackend
from yolov6.data.data_augment import letterbox
from yolov6.utils.nms import non_max_suppression

import image_pre

# If an input image is smaller than the segmentation model input size
# times the no-detection threshold the object detection model won't
# run and the full image will be forwarded to the segmentation model.
# This is useful for small images that can be fully fed in one pass
# of the segmentation model
NO_DETECTION_THRESHOLD = 3

def from_inferer_coords_to_image_coords(inferer_coords, image_size, margin=.15):
  x,y,w,h = inferer_coords
  px = x*image_size[0]
  py = y*image_size[1]
  w = w*image_size[0]
  h = h*image_size[1]
  px -= w/2 + w*margin
  py -= h/2 + h*margin
  w *= (1+2*margin)
  h *= (1+2*margin)
  px = int(max(px, 0))
  py = int(max(py, 0))
  w = int(min(w, image_size[0]-px))
  h = int(min(h, image_size[1]-py))
  return (px,py,w,h)

def collect_boxes_in_picture(picture, inferer):
  S = 2
  target_size = (640*S, 640*S)
  all_boxes = []
  for x,y,img in image_pre.iter_scaled_sections_of_image(picture, target_size):
    boxes = collect_boxes_in_section(inferer, img, margin=.2)
    for (bx,by,bw,bh) in boxes:
      (segx, segy), (segw, segh) = image_pre.upscale_scaled_size(picture, target_size, x+bx, y+by, bw, bh)
      all_boxes.append((segx, segy, segw, segh))
  return all_boxes

def collect_boxes_in_section(inferer, image, margin=.15):
  conf_thres = .25
  iou_thres = .45
  cv2_image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
  detected_boxes = inferer.infer(cv2_image, conf_thres, iou_thres)
  boxes = []
  for (detected_type, x,y,w,h, condidence_level) in detected_boxes:
    boxes.append(from_inferer_coords_to_image_coords((x,y,w,h), image.size, margin))
  return boxes

def merge_boxes(boxes, segmentation_model_input_size):
  def overlap(b1, b2):
    return b1[0] < b2[0]+b2[2] and b1[0]+b1[2] > b2[0] and b1[1] < b2[1]+b2[3] and b1[1]+b1[3] > b2[1]

  def merge(b1, b2):
    x1,y1,w1,h1 = b1
    x2,y2,w2,h2 = b2
    x = min(x1, x2)
    y = min(y1, y2)
    w = max(x1+w1, x2+w2) - x
    h = max(y1+h1, y2+h2) - y
    return (x, y, w, h)

  def area(b):
    return b[2]*b[3]

  def try_merge(merged_boxes, segmentation_model_input_size):
    for i, b1 in enumerate(merged_boxes):
      for j, b2 in enumerate(merged_boxes[:i]):
        if overlap(b1, b2):
          merged = merge(b1, b2)
          if area(merged) < (area(b1)+area(b2))*1.2 and\
            merged[2] < segmentation_model_input_size[0] and\
            merged[3] < segmentation_model_input_size[1]:
            merged_boxes[i] = merged
            del merged_boxes[j]
            return True
    return False

  while try_merge(boxes, segmentation_model_input_size):
    pass

  return boxes

def place_boxes_on_quad(boxes):
  widths  = list(map(lambda box: box[2], boxes))
  heights = list(map(lambda box: box[3], boxes))
  packed_coords = rpack.pack(zip((w+20 for w in widths), (h+20 for h in heights)))
  packed_image_size = (max(x+w for (x,y),w in zip(packed_coords, widths)), max(y+h for (x,y),h in zip(packed_coords, heights)))
  return packed_coords, packed_image_size

def compute_square_factor(size):
  """
  Returns
  -------
  A score, ranking from 0 to -inf, the higher means the "squarer"
  """
  highest_aspect_ratio = max(*size)/min(*size)
  return 1-highest_aspect_ratio


class Inferer():
  ###
  ### Based on yolov6/code/inferer
  ###

  def __init__(self, weights, yaml, image_size):
    self.__dict__.update(locals())
    self.device = 'cpu'
    self.image_size = image_size
    self.device = torch.device(self.device)
    self.model = DetectBackend(weights, device=self.device)
    self.stride = self.model.stride
    self.class_names = load_yaml(yaml)['names']
    self.image_size = self.check_image_size(self.image_size, s=self.stride)
    self.model.model.float()
    self.detected_classes = [self.class_names.index(clazz) for clazz in ('car', 'person', 'motorcycle', 'truck', 'bus')]

  def infer(self, image, conf_threshold, iou_threshold):
    ''' Model Inference and results visualization '''

    precessed_image = self.precess_image(image, self.image_size, self.stride)
    precessed_image = precessed_image.to(self.device)
    if len(precessed_image.shape) == 3:
      precessed_image = precessed_image[None]
    prediction_results = self.model(precessed_image)
    det = non_max_suppression(prediction_results, conf_threshold, iou_threshold, self.detected_classes, max_det=1000)[0]

    gn = torch.tensor(image.shape)[[1, 0, 1, 0]]

    boxes = []

    if len(det):
      det[:, :4] = self.rescale(precessed_image.shape[2:], det[:, :4], image.shape).round()

      for *xyxy, confidence, cls in reversed(det):
        xywh = (self.box_convert(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
        boxes.append((cls, *xywh, confidence))

    return boxes


  @staticmethod
  def precess_image(image, image_size, stride):
      image = letterbox(image, image_size, stride=stride)[0]

      image = image.transpose((2, 0, 1))[::-1]
      image = torch.from_numpy(np.ascontiguousarray(image))
      image = image.float()
      image /= 255

      return image


  @staticmethod
  def rescale(original_shape, boxes, target_shape):
    '''Rescale the output to the original image shape'''
    ratio = min(original_shape[0] / target_shape[0], original_shape[1] / target_shape[1])
    padding = (original_shape[1] - target_shape[1] * ratio) / 2, (original_shape[0] - target_shape[0] * ratio) / 2

    boxes[:, [0, 2]] -= padding[0]
    boxes[:, [1, 3]] -= padding[1]
    boxes[:, :4] /= ratio

    boxes[:, 0].clamp_(0, target_shape[1])
    boxes[:, 1].clamp_(0, target_shape[0])
    boxes[:, 2].clamp_(0, target_shape[1])
    boxes[:, 3].clamp_(0, target_shape[0])

    return boxes

  def check_image_size(self, img_size, s=32, floor=0):
    """Make sure image size is a multiple of stride s in each dimension, and return a new shape list of image."""
    if isinstance(img_size, int):
      new_size = max(self.make_divisible(img_size, int(s)), floor)
    elif isinstance(img_size, list):
      new_size = [max(self.make_divisible(x, int(s)), floor) for x in img_size]
    else:
      raise Exception(f"Unsupported type of img_size: {type(img_size)}")

    if new_size != img_size:
      print(f'WARNING: --img-size {img_size} must be multiple of max stride {s}, updating to {new_size}')
    return new_size if isinstance(img_size, list) else [new_size]*2

  def make_divisible(self, x, divisor):
    return math.ceil(x / divisor) * divisor

  
  @staticmethod
  def box_convert(x):
    y = x.clone() if isinstance(x, torch.Tensor) else np.copy(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2
    y[:, 2] = x[:, 2] - x[:, 0]
    y[:, 3] = x[:, 3] - x[:, 1]
    return y


def prepare_model():
  global actual_model
  actual_model = __import__(benchmark_name.split('@')[1], globals(), locals())
  weights = os.path.join(YOLOV6_PATH, 'yolov6s.pt')
  yaml = os.path.join(YOLOV6_PATH, 'data', 'coco.yaml')
  img_size = 640
  conf_thres = .25
  iou_thres = .45

  inferer = Inferer(weights, yaml, img_size)
  model = actual_model.prepare_model()
  return (inferer, model)

def compute_blur_mask(picture, inferer_and_model):
  inferer, model = inferer_and_model
  model_input_size = (513, 513) # hard coded to match the model's input size

  if picture.size[0] <= NO_DETECTION_THRESHOLD * model_input_size[0] or picture.size[1] <= NO_DETECTION_THRESHOLD * model_input_size[1]:
    # do not run the object detector if the original image is small enough
    # to be fed directly to the segmentation model
    return actual_model.compute_blur_mask(picture, model)
  else:
    all_boxes = collect_boxes_in_picture(picture, inferer)
    merged_boxes = merge_boxes(all_boxes[:], model_input_size)

    all_boxes_coords, all_boxes_quad_size = place_boxes_on_quad(all_boxes)
    merged_boxes_coords, merged_boxes_quad_size = place_boxes_on_quad(merged_boxes)

    # pick the best set of boxes
    packed_coords = all_boxes_coords
    packed_image_size = all_boxes_quad_size
    if compute_square_factor(merged_boxes_quad_size) > compute_square_factor(packed_image_size):        # pick the "squarest", that will be easier for the segmenter to digest
    # if merged_boxes_quad_size[0]/merged_boxes_quad_size[1] < packed_image_size[0]*packed_image_size[1]: # pick the smallest
      packed_coords = merged_boxes_coords
      packed_image_size = merged_boxes_quad_size
    
    # run the sementic segmenter on the packed boxes
    packed_image = Image.new('RGB', packed_image_size)
    for (x,y,w,h),(px,py) in zip(all_boxes, packed_coords):
      packed_image.paste(picture.crop((x,y,x+w,y+h)), (px,py))
    blurred_packed = actual_model.compute_blur_mask(packed_image, model)

    # unpack the boxes onto a blur mask of the original image
    blurred_mask = Image.new('RGBA', picture.size, (0,0,0,0))
    for (x,y,w,h),(px,py) in zip(all_boxes, packed_coords):
      crop = blurred_packed.crop((px,py,px+w,py+h))
      blurred_mask.paste(crop, (x,y,x+w,y+h), crop)

    return blurred_mask
